
// PARAMETERS & ARGUMENTS

function printName(name){
	console.log("My name is " + name);
}

printName("Juana");


function printMyage(age){
	console.log("I am " + age);
};
printMyage(24);
printMyage(25);

//  or function printMyage(age){
// 	console.log("I am " + age);
// };
// printMyage(24 + " years old.");



// DIVISIBILITY

function checkDivisibilityBy8(num){

	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);



// MINI ACTIVITY

function printMyFavoriteSuperHero(name){
	console.log("My favorite superhero is: " + name);
};
printMyFavoriteSuperHero("Wonder Woman");


function printMyFavoriteLanguage(language){
	console.log("My favorite language is: " + language);
}

printMyFavoriteLanguage("Javascript");
printMyFavoriteLanguage("Java");



// MULTIPLE ARGUMENTS & PARAMETERS

function printFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName ("Angel", "S.", "Labalan");




// USE VARIABLE AS ARGUMENTS

let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName, mName, lName);


// MINI ACTIVITY
function printMyFavoriteMusic(m1, m2, m3, m4, m5){
	console.log("These are my favorite songs: ");
	console.log(m1);
	console.log(m2);
	console.log(m3);
	console.log(m4);
	console.log(m5);
}

printMyFavoriteMusic ("Love came down on me", "For all  you've done", "Thank you Lord", "I just want to know you more", "with all I am");




// RETURN STATEMENT

let fullName = printFullName("Mark", "Joseph", "Lacdao");
console.log(fullName);



function returnFullName(firstName, middleName, lastName){

	return firstName + " " + middleName + " " + lastName;
}

fullName = returnFullName("Ernesto", "Antonio", "Maceda");
console.log(fullName);
console.log(fullName + "is my grandpa.")

// sample 1
function printMyFavoriteMusic(m1, m2, m3, m4, m5){
	console.log("These are my favorite songs: ");
	console.log(m1);
	console.log(m2);
	console.log(m3);
	console.log(m4);
	console.log(m5);

	return "ganda ako";
}

let sample1 = printMyFavoriteMusic ("Love came down on me", "For all  you've done", "Thank you Lord", "I just want to know you more", "with all I am");

console.log(sample1)

// sample 2
function returnPhilippineAddress(city){
	return city + ", Philippines"
}

let myFullAddress = returnPhilippineAddress("Cainta");
console.log(myFullAddress);


// sample 3
function checkDivisibilityBy4(num){

	let remainder = num % 4;

	let isDivisibleBy4 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	
	return isDivisibleBy4;
}

let num4isDivisibleBy4 = checkDivisibilityBy4(4);
console.log(num4isDivisibleBy4)


// MINI ACTIVITY

function createPlayerInfo(username,level,job){

	console.log ("username: " + username + ", level: ", level + ", job: " + job);
	return {username, level, job}

}
let user1 = createPlayerInfo("white_night",95,"Paladin");
console.log(user1);